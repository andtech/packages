# Setup

## NuGet
1. Add this project as a NuGet package source.

```
dotnet nuget add source "https://gitlab.com/api/v4/projects/29250253/packages/nuget/index.json" --name gitlab/andtech
```

# Installation
Install any of the available packages:

```
dotnet tool install -g andtech.dj
```

```
dotnet tool install -g andtech.to
```

```
dotnet tool install -g andtech.jot
```

```
dotnet tool install -g andtech.snip
```

```
dotnet tool install -g andtech.dpm
```

# FAQ
## Why not use GitHub Package Registry instead of GitLab?
* [GitHub Package Registry](https://github.com/features/packages) does not allow anonymous read-access for NuGet packages. This introduces the hassle of creating and managing personal access tokens. [GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) does not have this restriction.

## Why is the source code hosted on GitHub instead of GitLab?
* This is a personal choice. I like keeping my open source projects on GitHub and all my enterprise projects on GitLab.

## Links
* [GitHub](https://github.com/andtechstudios)
